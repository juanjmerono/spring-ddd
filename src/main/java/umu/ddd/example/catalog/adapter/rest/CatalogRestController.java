package umu.ddd.example.catalog.adapter.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import umu.ddd.example.catalog.application.usecases.GetCatalogUseCase;
import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Book;
import umu.ddd.example.catalog.domain.models.Catalog;

@RestController
@RequestMapping("/catalog")
public class CatalogRestController {
    
    @Autowired
    private GetCatalogUseCase getCatalogUseCase;

    @GetMapping("/")
    public Catalog getCatalog(
        @RequestParam(name="page",required = false, defaultValue = "1") int page,
        @RequestParam(name="search",required = false) String search) {
        try {
            return getCatalogUseCase.getPage(page,search);
        } catch (CatalogNotFoundException pnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable String id) {
        try {
            return getCatalogUseCase.getBookById(id);
        } catch (BookNotFoundException pnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
