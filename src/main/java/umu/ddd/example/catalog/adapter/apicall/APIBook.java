package umu.ddd.example.catalog.adapter.apicall;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.catalog.domain.models.Book;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class APIBook {
    private int id;
    private String title;
    private List<APIAuthor> authors;

    public Book toBook() {
        return Book.createBook(this.id, this.title, authors.stream().map(APIAuthor::toAuthor).collect(Collectors.toList()));
    }
}
