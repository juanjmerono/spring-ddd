package umu.ddd.example.catalog.adapter.apicall;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CatalogPersistenceUtils {
    
    @Value("${gutendex.api.url}")
    private String apiGutendex;

    public String getAPIUrl() { return apiGutendex; }

    public String convertAPIUrl(String url) {
        return (url!=null)?url.replace(apiGutendex,"/catalog"):url;
    }

}
