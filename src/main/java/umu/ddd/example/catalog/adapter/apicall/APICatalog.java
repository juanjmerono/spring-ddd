package umu.ddd.example.catalog.adapter.apicall;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.catalog.domain.models.Catalog;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class APICatalog {
    private int count;
    private String next;
    private String previous;
    private List<APIBook> results;

    public Catalog toCatalog(CatalogPersistenceUtils utils) {
        return Catalog.createCatalog(this.count, 
            utils.convertAPIUrl(this.next), 
            utils.convertAPIUrl(this.previous), 
            results.stream().map(APIBook::toBook).collect(Collectors.toList()));
    }
}
