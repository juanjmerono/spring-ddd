package umu.ddd.example.catalog.adapter.apicall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import umu.ddd.example.catalog.application.port.CatalogPersistence;
import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Catalog;

@Service
public class CatalogPersistenceImpl implements CatalogPersistence {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CatalogPersistenceUtils catalogPersistenceUtils;

    @Override
    public Catalog loadPage(int page, String search) throws CatalogNotFoundException {
        try {
            String apiQuery = catalogPersistenceUtils.getAPIUrl()+"/?page="+page+(search!=null?"&search="+search:"");
            return restTemplate
                .getForEntity(apiQuery, APICatalog.class)
                .getBody().toCatalog(catalogPersistenceUtils);
        } catch (NotFound nfe) {
            throw new CatalogNotFoundException();
        }
    }

    @Override
    public Catalog loadBookById(String id) throws BookNotFoundException {
        try {
            String apiQuery = catalogPersistenceUtils.getAPIUrl()+"/?ids="+id;
            return restTemplate
                .getForEntity(apiQuery, APICatalog.class)
                .getBody().toCatalog(catalogPersistenceUtils);
        } catch (NotFound nfe) {
            throw new BookNotFoundException();
        }
    }
    
}
