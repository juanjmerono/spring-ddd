package umu.ddd.example.catalog.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    private String name;
    private int born;
    private int die;

    public static Author createAuthor(String name, int birthYear, int deathYear) {
        return Author.builder()
            .name(name)
            .born(birthYear)
            .die(deathYear)
            .build();
    }
}
