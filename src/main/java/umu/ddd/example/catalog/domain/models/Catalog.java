package umu.ddd.example.catalog.domain.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Catalog {
    
    private int count;
    private String next;
    private String previous;
    private List<Book> books;

    public static Catalog createCatalog(int count, String nxt, String prv, List<Book> books) {
        return Catalog.builder()
            .count(count)
            .next(nxt)
            .previous(prv)
            .books(books)
            .build();
    }
}
