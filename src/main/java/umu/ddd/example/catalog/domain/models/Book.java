package umu.ddd.example.catalog.domain.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    
    private int id;
    private String title;
    private List<Author> authors;

    public static Book createBook(int id, String title, List<Author> auths) {
        return Book.builder()
            .id(id)
            .title(title)
            .authors(auths)
            .build();
    }
}
