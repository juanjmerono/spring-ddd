package umu.ddd.example.catalog.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.catalog.application.port.CatalogPersistence;
import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Book;
import umu.ddd.example.catalog.domain.models.Catalog;

@Service
public class GetCatalogUseCase {

    @Autowired
    CatalogPersistence catalogPersistence;

    public Catalog getPage(int page, String search) throws CatalogNotFoundException {
        Catalog c = catalogPersistence.loadPage(page, search);
        if (c.getCount()==0) throw new CatalogNotFoundException();
        return c;
    }

    public Book getBookById(String id) throws BookNotFoundException {
        Catalog c = catalogPersistence.loadBookById(id);
        if (c.getCount()==0) throw new BookNotFoundException();
        return c.getBooks().get(0);
    }

}
