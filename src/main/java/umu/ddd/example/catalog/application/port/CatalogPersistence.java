package umu.ddd.example.catalog.application.port;

import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Catalog;

public interface CatalogPersistence {
    public Catalog loadPage(int page, String search) throws CatalogNotFoundException;
    public Catalog loadBookById(String id) throws BookNotFoundException;
}
