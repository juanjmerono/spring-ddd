package umu.ddd.example.users.application.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.users.application.port.UserEventPublisher;
import umu.ddd.example.users.application.port.UserPersistence;
import umu.ddd.example.users.domain.events.NewUserEvent;
import umu.ddd.example.users.domain.exceptions.InvalidNickNameException;
import umu.ddd.example.users.domain.models.User;

@Service
public class RegisterUserUseCase {

    @Autowired
    private UserPersistence userPersistence;

    @Autowired
    private UserEventPublisher eventPublisher;

    public User registerUser(String nick) throws InvalidNickNameException {
        User usr = userPersistence.findByNickName(nick);
        if (usr==null) {
            usr = User.createUser(nick);
            userPersistence.save(usr);
            eventPublisher.userCreated(NewUserEvent.createEvent(usr));
        }
        return usr;
    }

}
