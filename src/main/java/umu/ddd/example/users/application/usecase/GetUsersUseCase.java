package umu.ddd.example.users.application.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.users.application.port.UserPersistence;
import umu.ddd.example.users.domain.exceptions.MembersNotFoundException;
import umu.ddd.example.users.domain.exceptions.UserNotFoundException;
import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

@Service
public class GetUsersUseCase {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private UserPersistence userPersistence;

    public Members getPage(int page, String search) throws MembersNotFoundException {
        if (page < 0) throw new MembersNotFoundException();
        return search!=null ? userPersistence.findAllStartsWith(page,search, PAGE_SIZE) : userPersistence.findAll(page,PAGE_SIZE);
    }

    public User getUserById(String id) throws UserNotFoundException {
        return userPersistence.findUserById(id);
    }
    
}
