package umu.ddd.example.users.application.port;

import umu.ddd.example.users.domain.exceptions.MembersNotFoundException;
import umu.ddd.example.users.domain.exceptions.UserNotFoundException;
import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

public interface UserPersistence {
    public void save(User user);
    public User findByNickName(String nick);
    public Members findAllStartsWith(int page,String search, int page_size) throws MembersNotFoundException ;
    public Members findAll(int page, int page_size) throws MembersNotFoundException ;
    public User findUserById(String id) throws UserNotFoundException;
}
