package umu.ddd.example.users.application.port;

import umu.ddd.example.users.domain.events.NewUserEvent;

public interface UserEventPublisher {
    public void userCreated(NewUserEvent event);
}
