package umu.ddd.example.users.domain.models;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.users.domain.exceptions.InvalidNickNameException;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String id;
    private String nickName;
    private Date created;

    public static User createUser(String nick) throws InvalidNickNameException {
        // Only allow not null nicks with 6 or more characters
        if (nick==null || nick.length()<6) throw new InvalidNickNameException();
        return User.builder()
                .id(UUID.randomUUID().toString())
                .nickName(nick)
                .build();
    }
}
