package umu.ddd.example.users.domain.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Members {
    private long count;
    private String next;
    private String previous;
    private List<User> users;

    public static Members createMember(long cnt, String nxt, String prv, List<User> userList) {
        return Members.builder()
                .count(cnt)
                .next(nxt)
                .previous(prv)
                .users(userList)
                .build();       
    }
}
