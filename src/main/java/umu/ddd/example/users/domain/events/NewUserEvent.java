package umu.ddd.example.users.domain.events;

import umu.ddd.example.users.domain.models.User;

public class NewUserEvent extends UserEvent {
    public NewUserEvent(User usr) {
        super(usr);
    }
    public static NewUserEvent createEvent(User usr) {
        return new NewUserEvent(usr);
    }
}
