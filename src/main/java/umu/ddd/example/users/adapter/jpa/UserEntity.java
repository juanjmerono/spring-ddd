package umu.ddd.example.users.adapter.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.users.domain.models.User;

@Data
@Builder
@Entity
@Immutable
@Table(name = "LIBRARY_USERS")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    @Column
    private String id;

    @Column
    private String nickName;
    
    public User toUser() {
        return User.builder()
            .id(this.getId())
            .nickName(this.nickName)
            .build();
    }

}
