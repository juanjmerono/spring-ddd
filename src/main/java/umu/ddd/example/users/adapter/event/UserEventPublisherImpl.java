package umu.ddd.example.users.adapter.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import umu.ddd.example.users.application.port.UserEventPublisher;
import umu.ddd.example.users.domain.events.NewUserEvent;

@Service
public class UserEventPublisherImpl implements UserEventPublisher {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public void userCreated(NewUserEvent event) {
        eventPublisher.publishEvent(new UserEventImpl(event));
    }

    
}
