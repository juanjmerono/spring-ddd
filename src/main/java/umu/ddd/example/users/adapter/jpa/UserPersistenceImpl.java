package umu.ddd.example.users.adapter.jpa;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import umu.ddd.example.users.application.port.UserPersistence;
import umu.ddd.example.users.domain.exceptions.MembersNotFoundException;
import umu.ddd.example.users.domain.exceptions.UserNotFoundException;
import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

@Service
public class UserPersistenceImpl implements UserPersistence {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user) {
        userRepository.save(UserEntity.builder()
            .id(user.getId())
            .nickName(user.getNickName())
            .build());
    }

    @Override
    public User findByNickName(String nick) {
        Page<UserEntity> pag = userRepository.findByNickNameStartsWith(nick,PageRequest.of(0,1));
        return (pag!=null && !pag.isEmpty())?pag.getContent().get(0).toUser():null;
    }

    @Override
    public User findUserById(String id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(()->new UserNotFoundException()).toUser();
    }

    private Members getMembersFromPage(Page<UserEntity> pag, String nextLink, String previousLink) throws MembersNotFoundException {
        if (pag!=null && pag.getTotalElements()>0) {
            return Members.createMember(pag.getTotalElements(), 
                pag.isLast()?null:nextLink, 
                pag.isFirst()?null:previousLink, 
                pag.getContent().stream().map(UserEntity::toUser).collect(Collectors.toList()));
        } else {
            throw new MembersNotFoundException();
        }
    }

    @Override
    public Members findAllStartsWith(int page, String search, int page_size) throws MembersNotFoundException {
        Page<UserEntity> pag = userRepository.findByNickNameStartsWith(search,PageRequest.of(page,page_size));
        String formatPageLink = "/users/?page=%s&search=%s";
        return getMembersFromPage(pag,String.format(formatPageLink,page+1,search),String.format(formatPageLink, page-1, search));
    }

    @Override
    public Members findAll(int page, int page_size) throws MembersNotFoundException {
        Page<UserEntity> pag = userRepository.findAll(PageRequest.of(page,page_size));
        String formatPageLink = "/users/?page=%s";
        return getMembersFromPage(pag,String.format(formatPageLink,page+1),String.format(formatPageLink, page-1));
    }
    
}
