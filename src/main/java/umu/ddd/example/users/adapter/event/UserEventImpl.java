package umu.ddd.example.users.adapter.event;

import org.springframework.context.ApplicationEvent;

import umu.ddd.example.users.domain.events.NewUserEvent;
import umu.ddd.example.users.domain.events.UserEvent;
import umu.ddd.example.users.domain.models.User;

public class UserEventImpl extends ApplicationEvent {

    public UserEventImpl(Object source) {
        super(source);
    }

    public UserEvent getDomainEvent() {
        return (UserEvent)this.getSource();
    }

    public User getUser() {
        return this.getDomainEvent().getUser();
    }

    public boolean isNewUserEvent() {
        return source instanceof NewUserEvent;
    }
    
}
