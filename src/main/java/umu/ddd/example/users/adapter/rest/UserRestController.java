package umu.ddd.example.users.adapter.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import umu.ddd.example.users.application.usecase.GetUsersUseCase;
import umu.ddd.example.users.application.usecase.RegisterUserUseCase;
import umu.ddd.example.users.domain.exceptions.InvalidNickNameException;
import umu.ddd.example.users.domain.exceptions.MembersNotFoundException;
import umu.ddd.example.users.domain.exceptions.UserNotFoundException;
import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

@RestController
@RequestMapping("/users")
public class UserRestController {
    
    @Autowired
    private GetUsersUseCase getUsersUseCase;

    @Autowired
    private RegisterUserUseCase registerUserUseCase;

    @PutMapping("/add")
    public User addUser(@RequestParam(name="nick",required = true) String nick) {
        try {
            return registerUserUseCase.registerUser(nick);
        } catch (InvalidNickNameException inn) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @GetMapping("/")
    public Members getUsers(
        @RequestParam(name="page",required = false, defaultValue = "0") int page,
        @RequestParam(name="search",required = false) String search) {
        try {
            return getUsersUseCase.getPage(page, search);
        } catch (MembersNotFoundException mnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable String id) {
        try {
            return getUsersUseCase.getUserById(id);
        } catch (UserNotFoundException pnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
