package umu.ddd.example.users.adapter.jpa;

import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity,String>{
    public Page<UserEntity> findByNickNameStartsWith(String nick, Pageable pageable);
}
