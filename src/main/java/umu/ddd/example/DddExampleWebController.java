package umu.ddd.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web")
public class DddExampleWebController {
    
    @GetMapping({"/","/catalog","/users","/stock","/renting"})
    public String getIndexPage() {
        return "index.html";
    }

}
