package umu.ddd.example.stock.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.stock.application.port.StockPersistence;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Stock;

@Service
public class GetStockUseCase {

    private final static int PAGE_SIZE = 10;

    @Autowired
    private StockPersistence stockPersistence;

    public Stock getPage(int page, String search) throws StockNotFoundException {
        if (page<0) throw new StockNotFoundException();
        return search!=null ? stockPersistence.findAllStartsWith(page, search, PAGE_SIZE): stockPersistence.findAll(page, PAGE_SIZE);
    }
    
}
