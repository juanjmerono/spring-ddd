package umu.ddd.example.stock.application.port;

import umu.ddd.example.stock.domain.events.DecreaseStockEvent;
import umu.ddd.example.stock.domain.events.EmptyStockEvent;
import umu.ddd.example.stock.domain.events.IncreaseStockEvent;
import umu.ddd.example.stock.domain.events.ResetStockEvent;
import umu.ddd.example.stock.domain.events.UpdateStockTitleEvent;

public interface StockEventPublisher {
    public void stockChanged(ResetStockEvent event);
    public void stockTitleChanged(UpdateStockTitleEvent event);
    public void increaseStock(IncreaseStockEvent event);
    public void decreaseStock(DecreaseStockEvent event);
    public void emptyStock(EmptyStockEvent event);
}
