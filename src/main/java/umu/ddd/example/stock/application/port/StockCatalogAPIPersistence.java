package umu.ddd.example.stock.application.port;

import umu.ddd.example.stock.domain.exceptions.BookNotFoundException;

public interface StockCatalogAPIPersistence {
    public String getBookTitle(String bookId) throws BookNotFoundException;
}
