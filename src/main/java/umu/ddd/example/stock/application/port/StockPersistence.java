package umu.ddd.example.stock.application.port;

import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;
import umu.ddd.example.stock.domain.models.Stock;

public interface StockPersistence {
    public Stock findAll(int page, int page_size) throws StockNotFoundException;
    public Stock findAllStartsWith(int page, String search, int page_size) throws StockNotFoundException;
    public Book updateStock(String bookId, int count);
    public Book updateBookTitle(String bookId, String title) throws StockNotFoundException;
    public Book increaseStock(String bookId) throws StockNotFoundException;
    public Book decreaseStock(String bookId) throws StockNotFoundException, LimitStockException;
    public void removeStock(String bookId);
}
