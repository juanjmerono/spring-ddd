package umu.ddd.example.stock.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.stock.application.port.StockCatalogAPIPersistence;
import umu.ddd.example.stock.application.port.StockEventPublisher;
import umu.ddd.example.stock.application.port.StockPersistence;
import umu.ddd.example.stock.domain.events.DecreaseStockEvent;
import umu.ddd.example.stock.domain.events.EmptyStockEvent;
import umu.ddd.example.stock.domain.events.IncreaseStockEvent;
import umu.ddd.example.stock.domain.events.ResetStockEvent;
import umu.ddd.example.stock.domain.events.UpdateStockTitleEvent;
import umu.ddd.example.stock.domain.exceptions.BookNotFoundException;
import umu.ddd.example.stock.domain.exceptions.InvalidStockException;
import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;

@Service
public class UpdateStockUseCase {

    @Autowired
    private StockPersistence stockPersistence;

    @Autowired
    private StockEventPublisher eventPublisher;

    @Autowired
    private StockCatalogAPIPersistence bookPersistence;

    public Book changeStock(String bookId, int count) throws InvalidStockException {
        if (count<=0) throw new InvalidStockException();
        Book book = stockPersistence.updateStock(bookId,count);
        eventPublisher.stockChanged(ResetStockEvent.createEvent(book));
        return book;
    }

    public void updateBookTitle(String bookId) throws StockNotFoundException {
        try {
            Book book = stockPersistence.updateBookTitle(bookId,bookPersistence.getBookTitle(bookId));
            eventPublisher.stockTitleChanged(UpdateStockTitleEvent.createEvent(book));
        } catch (BookNotFoundException e) {
            stockPersistence.removeStock(bookId);
            throw new StockNotFoundException();
        } catch (StockNotFoundException e) {
            throw e;
        }
        
    }

    public Book increaseStock(String bookId) throws StockNotFoundException {
        try {
            Book book = stockPersistence.increaseStock(bookId);
            eventPublisher.increaseStock(IncreaseStockEvent.createEvent(book));
            return book;
        } catch (StockNotFoundException e) {
            throw e;
        }
    }

    public Book decreaseStock(String bookId) throws StockNotFoundException, LimitStockException {
        try {
            Book book = stockPersistence.decreaseStock(bookId);
            eventPublisher.decreaseStock(DecreaseStockEvent.createEvent(book));
            if (book.getCount()==0) eventPublisher.emptyStock(EmptyStockEvent.createEvent(book));
            return book;
        } catch (StockNotFoundException | LimitStockException e) {
            throw e;
        }
    }
    
}
