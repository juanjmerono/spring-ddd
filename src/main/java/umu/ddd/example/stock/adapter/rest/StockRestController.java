package umu.ddd.example.stock.adapter.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import umu.ddd.example.stock.application.usecases.GetStockUseCase;
import umu.ddd.example.stock.application.usecases.UpdateStockUseCase;
import umu.ddd.example.stock.domain.exceptions.InvalidStockException;
import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;
import umu.ddd.example.stock.domain.models.Stock;

@RestController
@RequestMapping("/stock")
public class StockRestController {
    @Autowired
    private GetStockUseCase getStockUseCase;

    @Autowired
    private UpdateStockUseCase updateStockUseCase;

    @PutMapping("/update")
    public Book updateStock(
        @RequestParam(name="book",required = true) String bookId,
        @RequestParam(name="count",required = true) int count) {
        try {
            return updateStockUseCase.changeStock(bookId, count);
        } catch (InvalidStockException mnf) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @GetMapping("/")
    public Stock getUsers(
        @RequestParam(name="page",required = false, defaultValue = "0") int page,
        @RequestParam(name="search",required = false) String search) {
        try {
            return getStockUseCase.getPage(page, search);
        } catch (StockNotFoundException mnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/increase")
    public Book increaseStock(
        @RequestParam(name="bookid",required = true) String bookId) {
        try {
            return updateStockUseCase.increaseStock(bookId);
        } catch (StockNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/decrease")
    public Book decreaseStock(
        @RequestParam(name="bookid",required = true) String bookId) {
        try {
            return updateStockUseCase.decreaseStock(bookId);
        } catch (StockNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (LimitStockException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

}
