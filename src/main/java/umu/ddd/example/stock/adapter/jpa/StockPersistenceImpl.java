package umu.ddd.example.stock.adapter.jpa;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import umu.ddd.example.stock.application.port.StockPersistence;
import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;
import umu.ddd.example.stock.domain.models.Stock;

@Service
public class StockPersistenceImpl implements StockPersistence {

    @Autowired
    private StockRepository stockRepository;

    private Stock getStockFromPage(Page<BookEntity> pag, String nxt, String prv) throws StockNotFoundException {
        if (pag!=null && pag.getTotalElements()>0) {
            return Stock.createStock(pag.getTotalElements(), 
                                    pag.isLast()?null:nxt, 
                                    pag.isFirst()?null:prv, 
                                    pag.getContent().stream().map(BookEntity::toBook).collect(Collectors.toList()));
        } else {
            throw new StockNotFoundException();
        }
    }

    @Override
    public Stock findAll(int page, int page_size) throws StockNotFoundException {
        Page<BookEntity> pag = stockRepository.findAll(PageRequest.of(page,page_size));
        String formatPageLink = "/stock/?page=%s";
        return getStockFromPage(pag,String.format(formatPageLink,page+1),String.format(formatPageLink, page-1));
    }

    @Override
    public Stock findAllStartsWith(int page, String search, int page_size) throws StockNotFoundException {
        Page<BookEntity> pag = stockRepository.findByBookIdStartsWith(search,PageRequest.of(page,page_size));
        String formatPageLink = "/stock/?page=%s&search=%s";
        return getStockFromPage(pag,String.format(formatPageLink,page+1,search),String.format(formatPageLink, page-1, search));
    }

    @Override
    public Book updateStock(String bookId, int count) {
        BookEntity book = stockRepository.findById(bookId).orElse(BookEntity.builder().bookId(bookId).title("...").build());
        book.setCount(count);
        stockRepository.save(book);
        return book.toBook();
    }

    @Override
    public Book updateBookTitle(String bookId, String title) throws StockNotFoundException {
        BookEntity book = stockRepository.findById(bookId).orElseThrow(() -> new StockNotFoundException());
        book.setTitle(title);
        stockRepository.save(book);
        return book.toBook();
    }

    @Override
    public Book increaseStock(String bookId) throws StockNotFoundException {
        BookEntity book = stockRepository.findById(bookId).orElseThrow(()->new StockNotFoundException());
        book.setCount(book.getCount()+1);
        stockRepository.save(book);
        return book.toBook();
    }

    @Override
    public Book decreaseStock(String bookId) throws StockNotFoundException, LimitStockException {
        BookEntity book = stockRepository.findById(bookId).orElseThrow(()->new StockNotFoundException());
        if (book.getCount()>0) {
            book.setCount(book.getCount()-1);
            stockRepository.save(book);
        } else {
            throw new LimitStockException();
        }
        return book.toBook();
    }

    @Override
    public void removeStock(String bookId) {
        stockRepository.deleteById(bookId);
    }

}
