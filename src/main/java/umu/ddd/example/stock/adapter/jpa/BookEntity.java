package umu.ddd.example.stock.adapter.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.stock.domain.models.Book;

@Data
@Builder
@Entity
@Table(name = "LIBRARY_STOCK")
@NoArgsConstructor
@AllArgsConstructor
public class BookEntity {

    @Id
    @Column
    private String bookId;

    @Column
    private String title;

    @Column
    private int count;
    
    public Book toBook() {
        return Book.createBook(this.bookId, this.title, this.count);
    }

}
