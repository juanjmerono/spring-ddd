package umu.ddd.example.stock.adapter.event;

import org.springframework.context.ApplicationEvent;

import umu.ddd.example.stock.domain.events.DecreaseStockEvent;
import umu.ddd.example.stock.domain.events.EmptyStockEvent;
import umu.ddd.example.stock.domain.events.IncreaseStockEvent;
import umu.ddd.example.stock.domain.events.ResetStockEvent;
import umu.ddd.example.stock.domain.events.StockEvent;
import umu.ddd.example.stock.domain.events.UpdateStockTitleEvent;
import umu.ddd.example.stock.domain.models.Book;

public class StockEventImpl extends ApplicationEvent {

    public StockEventImpl(Object source) {
        super(source);
    }

    public StockEvent getDomainEvent() {
        return (StockEvent)this.getSource();
    }

    public Book getBook() {
        return this.getDomainEvent().getBook();
    }

    public boolean isIncreaseStockEvent() {
        return this.source instanceof IncreaseStockEvent;
    }

    public boolean isDecreaseStockEvent() {
        return this.source instanceof DecreaseStockEvent;
    }

    public boolean isEmptyStockEvent() {
        return this.source instanceof EmptyStockEvent;
    }

    public boolean isResetStockEvent() {
        return this.source instanceof ResetStockEvent;
    }

    public boolean isUpdateStockTitleEvent() {
        return this.source instanceof UpdateStockTitleEvent;
    }

}
