package umu.ddd.example.stock.adapter.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import umu.ddd.example.stock.application.usecases.UpdateStockUseCase;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;

@Service
public class StockEventListener implements ApplicationListener<StockEventImpl> {

    @Autowired
    private UpdateStockUseCase updateStockUseCase;

    @Override
    public void onApplicationEvent(StockEventImpl event) {
        try {
            if (event.isResetStockEvent()) {
                updateStockUseCase.updateBookTitle(event.getBook().getBookId());
            }
        } catch (StockNotFoundException e) {
            // Nothing to do
        }
    }
    
}
