package umu.ddd.example.stock.adapter.apicall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import umu.ddd.example.stock.application.port.StockCatalogAPIPersistence;
import umu.ddd.example.stock.domain.exceptions.BookNotFoundException;

@Service
public class StockCatalogAPIPersistenceImpl implements StockCatalogAPIPersistence {
    
    @Autowired
    private RestTemplate restTemplate;

    @Value("${catalog.api.url}")
    private String apiBaseUrl;

    @Override
    public String getBookTitle(String bookId) throws BookNotFoundException {
        String apiQuery = apiBaseUrl+"/"+bookId;
        try {
            return restTemplate
                .getForEntity(apiQuery, APIBook.class)
                .getBody().getTitle();
        } catch (NotFound nfe) {
            throw new BookNotFoundException();
        }
    }

}
