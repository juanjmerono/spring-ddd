package umu.ddd.example.stock.adapter.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import umu.ddd.example.stock.application.port.StockEventPublisher;
import umu.ddd.example.stock.domain.events.DecreaseStockEvent;
import umu.ddd.example.stock.domain.events.EmptyStockEvent;
import umu.ddd.example.stock.domain.events.IncreaseStockEvent;
import umu.ddd.example.stock.domain.events.ResetStockEvent;
import umu.ddd.example.stock.domain.events.UpdateStockTitleEvent;

@Service
public class StockEventPublisherImpl implements StockEventPublisher {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public void stockChanged(ResetStockEvent event) {
        eventPublisher.publishEvent(new StockEventImpl(event));
    }

    @Override
    public void stockTitleChanged(UpdateStockTitleEvent event) {
        eventPublisher.publishEvent(new StockEventImpl(event));
    }

    @Override
    public void increaseStock(IncreaseStockEvent event) {
        eventPublisher.publishEvent(new StockEventImpl(event));
    }

    @Override
    public void decreaseStock(DecreaseStockEvent event) {
        eventPublisher.publishEvent(new StockEventImpl(event));
    }

    @Override
    public void emptyStock(EmptyStockEvent event) {
        eventPublisher.publishEvent(new StockEventImpl(event));
    }
    
}
