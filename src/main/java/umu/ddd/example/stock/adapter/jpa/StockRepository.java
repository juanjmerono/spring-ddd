package umu.ddd.example.stock.adapter.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends PagingAndSortingRepository<BookEntity,String>{
    public Page<BookEntity> findByBookIdStartsWith(String bookId, Pageable pageable);
}
