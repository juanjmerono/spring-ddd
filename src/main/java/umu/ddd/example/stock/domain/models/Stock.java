package umu.ddd.example.stock.domain.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stock {
    private long count;
    private String next;
    private String previous;
    private List<Book> books;

    public static Stock createStock(long count, String nxt, String prv, List<Book> books) {
        return Stock.builder()
            .count(count)
            .next(nxt)
            .previous(prv)
            .books(books)
            .build();       

    }
}
