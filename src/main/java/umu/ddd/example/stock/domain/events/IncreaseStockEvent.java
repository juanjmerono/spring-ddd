package umu.ddd.example.stock.domain.events;

import umu.ddd.example.stock.domain.models.Book;

public class IncreaseStockEvent extends StockEvent {

    public IncreaseStockEvent(Book book) {
        super(book);
    }

    public static IncreaseStockEvent createEvent(Book book) {
        return new IncreaseStockEvent(book);
    }
    
}
