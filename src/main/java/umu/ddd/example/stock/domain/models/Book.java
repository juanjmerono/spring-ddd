package umu.ddd.example.stock.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    private String bookId;
    private String title;
    private int count;

    public static Book createBook(String bookId, String title, int count) {
        return Book.builder()
            .bookId(bookId)
            .title(title)
            .count(count)
            .build();
    }
}
