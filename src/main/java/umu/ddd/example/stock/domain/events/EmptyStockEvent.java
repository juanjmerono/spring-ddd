package umu.ddd.example.stock.domain.events;

import umu.ddd.example.stock.domain.models.Book;

public class EmptyStockEvent extends StockEvent {

    public EmptyStockEvent(Book book) {
        super(book);
    }

    public static EmptyStockEvent createEvent(Book book) {
        return new EmptyStockEvent(book);
    }
    
}
