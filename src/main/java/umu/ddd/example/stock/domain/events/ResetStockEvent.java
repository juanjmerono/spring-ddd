package umu.ddd.example.stock.domain.events;

import umu.ddd.example.stock.domain.models.Book;

public class ResetStockEvent extends StockEvent {

    public ResetStockEvent(Book book) {
        super(book);
    }

    public static ResetStockEvent createEvent(Book book) {
        return new ResetStockEvent(book);
    }
    
}
