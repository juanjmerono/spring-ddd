package umu.ddd.example.stock.domain.events;

import umu.ddd.example.stock.domain.models.Book;

public class UpdateStockTitleEvent extends StockEvent {

    public UpdateStockTitleEvent(Book book) {
        super(book);
    }

    public static UpdateStockTitleEvent createEvent(Book book) {
        return new UpdateStockTitleEvent(book);
    }
    
}
