package umu.ddd.example.stock.domain.events;

import umu.ddd.example.stock.domain.models.Book;

public class DecreaseStockEvent extends StockEvent {

    public DecreaseStockEvent(Book book) {
        super(book);
    }

    public static DecreaseStockEvent createEvent(Book book) {
        return new DecreaseStockEvent(book);
    }
    
}
