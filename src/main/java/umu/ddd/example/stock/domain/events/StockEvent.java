package umu.ddd.example.stock.domain.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.stock.domain.models.Book;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockEvent {
    private Book book;    
}
