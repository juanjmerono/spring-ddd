package umu.ddd.example.renting.application.port;

import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.models.Lead;
import umu.ddd.example.renting.domain.models.Renting;

public interface RentingPersistence {
    public Renting findAllStartsWith(int page, String bookId, int page_size) throws RentingNotFoundException;
    public Renting findAll(int page, int page_size) throws RentingNotFoundException;
    public Lead startLead(String bookId, String userId, int maxRenting) throws DuplicateRentingException, LimitRentingException;
    public Lead returnLead(String bookId, String userId) throws RentingNotFoundException;
    public Lead udpateTitles(String bookId, String userId, String title, String nick) throws RentingNotFoundException;
}
