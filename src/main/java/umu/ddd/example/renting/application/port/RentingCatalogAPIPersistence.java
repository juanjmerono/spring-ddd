package umu.ddd.example.renting.application.port;

public interface RentingCatalogAPIPersistence {
    public String getBookTitle(String bookId) throws Exception;
}
