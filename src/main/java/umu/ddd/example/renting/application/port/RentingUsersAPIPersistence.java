package umu.ddd.example.renting.application.port;

public interface RentingUsersAPIPersistence {
    public String getUserNick(String userId) throws Exception;
}
