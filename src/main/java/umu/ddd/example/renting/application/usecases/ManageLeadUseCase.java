package umu.ddd.example.renting.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.port.RentingEventPublisher;
import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.application.port.RentingStockAPIPersistence;
import umu.ddd.example.renting.domain.events.ReturnLeadEvent;
import umu.ddd.example.renting.domain.events.StartLeadEvent;
import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.exceptions.RentingStockException;
import umu.ddd.example.renting.domain.models.Lead;

@Service
public class ManageLeadUseCase {

    private static final int MAX_RENTING = 2;

    @Autowired
    private RentingPersistence rentingPersistence;

    @Autowired
    private RentingStockAPIPersistence stockPersistence;

    @Autowired
    private RentingEventPublisher eventPublisher;

    public Lead startLead(String bookId, String userId) throws DuplicateRentingException, LimitRentingException, RentingStockException {
        // Decrease stock
        stockPersistence.decreaseStock(bookId);
        try {
            // Try to rent book
            Lead l = rentingPersistence.startLead(bookId, userId, MAX_RENTING);
            eventPublisher.startLead(StartLeadEvent.createEvent(l));
            return l;
        } catch (Exception ex) {
            // If fail return stock and raise excption
            stockPersistence.increaseStock(bookId);
            throw ex;    
        }
    }

    public Lead returnLead(String bookId, String userId) throws RentingNotFoundException, RentingStockException {
        // Increase stock
        stockPersistence.increaseStock(bookId);
        // Try to return book
        Lead l = rentingPersistence.returnLead(bookId,userId);
        eventPublisher.returnLead(ReturnLeadEvent.createEvent(l));
        return l;
    }
    
}
