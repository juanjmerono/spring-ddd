package umu.ddd.example.renting.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.models.Renting;

@Service
public class GetRentingUseCase {

    private static final int PAGE_SIZE = 10;
    @Autowired
    private RentingPersistence rentingPersistence;

    public Renting getPage(int page, String search) throws RentingNotFoundException {
        if (page<0) throw new RentingNotFoundException();
        return search!=null ? rentingPersistence.findAllStartsWith(page, search, PAGE_SIZE) : rentingPersistence.findAll(page, PAGE_SIZE);
    }

}
