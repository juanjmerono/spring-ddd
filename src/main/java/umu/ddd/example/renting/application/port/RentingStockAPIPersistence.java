package umu.ddd.example.renting.application.port;

import umu.ddd.example.renting.domain.exceptions.RentingStockException;

public interface RentingStockAPIPersistence {
    public boolean decreaseStock(String bookId) throws RentingStockException;
    public boolean increaseStock(String bookId) throws RentingStockException;
}
