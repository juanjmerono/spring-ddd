package umu.ddd.example.renting.application.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.port.RentingCatalogAPIPersistence;
import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.application.port.RentingUsersAPIPersistence;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.models.Lead;

@Service
public class UpdateTitlesUseCase {

    @Autowired
    private RentingPersistence rentingPersistence;

    @Autowired
    private RentingCatalogAPIPersistence catalogPersistence;

    @Autowired
    private RentingUsersAPIPersistence usersPersistence;

    public Lead udpateTitles(String bookId, String userId) throws RentingNotFoundException {
        String bookTitle = "unknown", nick = "unknown";
        try {
            bookTitle = catalogPersistence.getBookTitle(bookId);
            nick = usersPersistence.getUserNick(userId);
        } catch (Exception ex) {
            // Nothing to do
        }
        return rentingPersistence.udpateTitles(bookId, userId, bookTitle, nick);
    }
    
}
