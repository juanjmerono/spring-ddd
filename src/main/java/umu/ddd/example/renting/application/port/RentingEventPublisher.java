package umu.ddd.example.renting.application.port;

import umu.ddd.example.renting.domain.events.ReturnLeadEvent;
import umu.ddd.example.renting.domain.events.StartLeadEvent;

public interface RentingEventPublisher {
    public void startLead(StartLeadEvent event);
    public void returnLead(ReturnLeadEvent event);
}
