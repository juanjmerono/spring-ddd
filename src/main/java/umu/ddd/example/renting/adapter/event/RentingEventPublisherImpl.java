package umu.ddd.example.renting.adapter.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.port.RentingEventPublisher;
import umu.ddd.example.renting.domain.events.ReturnLeadEvent;
import umu.ddd.example.renting.domain.events.StartLeadEvent;

@Service
public class RentingEventPublisherImpl implements RentingEventPublisher {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public void startLead(StartLeadEvent event) {
        eventPublisher.publishEvent(new RentingEventImpl(event));
        
    }

    @Override
    public void returnLead(ReturnLeadEvent event) {
        eventPublisher.publishEvent(new RentingEventImpl(event));
    }
    
}
