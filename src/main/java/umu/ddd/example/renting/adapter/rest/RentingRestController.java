package umu.ddd.example.renting.adapter.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import umu.ddd.example.renting.application.usecases.GetRentingUseCase;
import umu.ddd.example.renting.application.usecases.ManageLeadUseCase;
import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.exceptions.RentingStockException;
import umu.ddd.example.renting.domain.models.Lead;
import umu.ddd.example.renting.domain.models.Renting;

@RestController
@RequestMapping("/renting")
public class RentingRestController {
    @Autowired
    private ManageLeadUseCase manageLeadUseCase;

    @Autowired
    private GetRentingUseCase getRentingUseCase;

    @PutMapping("/loan")
    public Lead startLead(
        @RequestParam(name="bookid",required = true) String bookId,
        @RequestParam(name="userid",required = true) String userId) {
        try {
            return manageLeadUseCase.startLead(bookId,userId);
        } catch (DuplicateRentingException | LimitRentingException | RentingStockException dre) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }    
            
    }

    @PutMapping("/return")
    public Lead returnLead(
        @RequestParam(name="bookid",required = true) String bookId,
        @RequestParam(name="userid",required = true) String userId) {
        try {
            return manageLeadUseCase.returnLead(bookId, userId);
        } catch (RentingNotFoundException | RentingStockException mnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }    
    }

    @GetMapping("/")
    public Renting getRentings(
        @RequestParam(name="page",required = false, defaultValue = "0") int page,
        @RequestParam(name="search",required = false) String search) {
        try {
            return getRentingUseCase.getPage(page, search);
        } catch (RentingNotFoundException mnf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
