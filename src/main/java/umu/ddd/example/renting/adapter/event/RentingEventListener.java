package umu.ddd.example.renting.adapter.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.usecases.UpdateTitlesUseCase;

@Service
public class RentingEventListener implements ApplicationListener<RentingEventImpl> {

    @Autowired
    private UpdateTitlesUseCase updateTitlesUseCase;

    @Override
    public void onApplicationEvent(RentingEventImpl event) {
        try {
            if (event.isStartLeadEvent()) {
                updateTitlesUseCase.udpateTitles(event.getLead().getBookId(),event.getLead().getUserId());
            }
        } catch (Exception ex) {
            // Nothing to do
        }
    }
    
}
