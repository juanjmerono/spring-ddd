package umu.ddd.example.renting.adapter.jpa;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.models.Lead;
import umu.ddd.example.renting.domain.models.Renting;

@Service
public class RentingPersistenceImpl implements RentingPersistence {

    @Autowired
    private RentingRepository rentingRepository;

    @Autowired
    private RentingUserRepository userRepository;

    @Autowired
    private RentingBookRepository bookRepository;

    private Renting getRentingFromPage(Page<RentingEntity> pag, String nxt, String prv) throws RentingNotFoundException {
        if (pag!=null && pag.getTotalElements()>0) {
            return Renting.createRenting(pag.getTotalElements(), 
                            pag.isLast()?null:nxt, 
                            pag.isFirst()?null:prv, 
                            pag.getContent().stream().map(RentingEntity::toLead).collect(Collectors.toList()));
        } else {
            throw new RentingNotFoundException();
        }
    }

    @Override
    public Renting findAllStartsWith(int page, String bookId, int page_size) throws RentingNotFoundException {
        Page<RentingEntity> pag = rentingRepository.findByRentingIdBookIdStartsWith(bookId,PageRequest.of(page,page_size));
        String formatPageLink = "/renting/?page=%s&search=%s";
        return getRentingFromPage(pag,String.format(formatPageLink,page+1,bookId),String.format(formatPageLink, page-1, bookId));
    }

    @Override
    public Renting findAll(int page, int page_size) throws RentingNotFoundException {
        Page<RentingEntity> pag = rentingRepository.findAll(PageRequest.of(page,page_size));
        String formatPageLink = "/renting/?page=%s";
        return getRentingFromPage(pag,String.format(formatPageLink,page+1),String.format(formatPageLink, page-1));
    }

    @Override
    public Lead startLead(String bookId, String userId, int max_renting) throws DuplicateRentingException, LimitRentingException {
        List<RentingEntity> userRentings = rentingRepository.findAllByRentingIdUserId(userId);
        if (userRentings.size()>=max_renting) throw new LimitRentingException();
        if (userRentings.stream().filter(r -> r.getRentingId().getBookId().equals(bookId)).count()>0) throw new DuplicateRentingException();
        RentingBookEntity bookEntity = bookRepository.findById(bookId).orElse(RentingBookEntity.builder().bookId(bookId).title("loading...").build());
        RentingUserEntity userEntity = userRepository.findById(userId).orElse(RentingUserEntity.builder().userId(userId).nick("loading...").build());
        RentingEntity rentingEntity = 
            RentingEntity.builder()
                .rentingId(RentingId.builder()
                    .bookId(bookId)
                    .userId(userId)
                    .build())
                .count(1)
                .book(bookEntity)
                .user(userEntity)
                .build();
        bookRepository.save(bookEntity);
        userRepository.save(userEntity);
        rentingRepository.save(rentingEntity);
        return rentingEntity.toLead();
    }

    @Override
    public Lead returnLead(String bookId, String userId) throws RentingNotFoundException {
        RentingEntity renting = rentingRepository
            .findById(RentingId.builder()
                .bookId(bookId)
                .userId(userId)
                .build())
            .orElseThrow(() -> new RentingNotFoundException());
        rentingRepository.delete(renting);
        renting.setCount(0);
        return renting.toLead();
    }

    @Override
    public Lead udpateTitles(String bookId, String userId, String title, String nick) throws RentingNotFoundException {
        RentingUserEntity user = userRepository.findById(userId).orElseThrow(() -> new RentingNotFoundException());
        RentingBookEntity book = bookRepository.findById(bookId).orElseThrow(() -> new RentingNotFoundException());
        user.setNick(nick);
        book.setTitle(title);
        userRepository.save(user);
        bookRepository.save(book);
        return Lead.createLead(bookId, userId, 1, title, nick);
    }
    
}
