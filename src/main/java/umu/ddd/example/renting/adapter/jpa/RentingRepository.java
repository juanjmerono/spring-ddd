package umu.ddd.example.renting.adapter.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentingRepository extends PagingAndSortingRepository<RentingEntity,RentingId>{
    public Page<RentingEntity> findByRentingIdBookIdStartsWith(String bookId, Pageable pageable);
    public List<RentingEntity> findAllByRentingIdUserId(String userId);
}
