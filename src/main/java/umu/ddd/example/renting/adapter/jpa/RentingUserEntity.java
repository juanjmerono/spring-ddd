package umu.ddd.example.renting.adapter.jpa;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name = "LIBRARY_RENT_USERS")
@NoArgsConstructor
@AllArgsConstructor
public class RentingUserEntity {

    @Id
    private String userId;

    @Column
    private String nick;

    @OneToMany(mappedBy = "user")
    Set<RentingEntity> rentings;

}
