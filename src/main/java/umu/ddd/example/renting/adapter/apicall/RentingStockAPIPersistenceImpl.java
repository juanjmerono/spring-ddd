package umu.ddd.example.renting.adapter.apicall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import umu.ddd.example.renting.application.port.RentingStockAPIPersistence;
import umu.ddd.example.renting.domain.exceptions.RentingStockException;

@Service
public class RentingStockAPIPersistenceImpl implements RentingStockAPIPersistence {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${stock.api.url}")
    private String apiBaseUrl;

    @Override
    public boolean decreaseStock(String bookId) throws RentingStockException {
        String apiQuery = apiBaseUrl+"/decrease?bookid="+bookId;
        try {
            return restTemplate
                .postForEntity(apiQuery, null, APIStock.class)
                .getBody().getCount() >= 0;
        } catch (Exception nfe) {
            throw new RentingStockException();
        }
    }

    @Override
    public boolean increaseStock(String bookId) throws RentingStockException {
        String apiQuery = apiBaseUrl+"/increase?bookid="+bookId;
        try {
            return restTemplate
                .postForEntity(apiQuery, null, APIStock.class)
                .getBody().getCount() >= 0;
        } catch (Exception nfe) {
            throw new RentingStockException();
        }
    }

}
