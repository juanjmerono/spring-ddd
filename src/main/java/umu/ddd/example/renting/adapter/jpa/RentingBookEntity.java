package umu.ddd.example.renting.adapter.jpa;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name = "LIBRARY_RENT_BOOKS")
@NoArgsConstructor
@AllArgsConstructor
public class RentingBookEntity {

    @Id
    private String bookId;

    @Column
    private String title;

    @OneToMany(mappedBy = "book")
    Set<RentingEntity> rentings;

}
