package umu.ddd.example.renting.adapter.jpa;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.renting.domain.models.Lead;

@Data
@Builder
@Entity
@Immutable
@Table(name = "LIBRARY_RENT")
@NoArgsConstructor
@AllArgsConstructor
public class RentingEntity {

    @EmbeddedId
    private RentingId rentingId;

    @ManyToOne
    @MapsId("userId")
    private RentingUserEntity user;

    @ManyToOne
    @MapsId("bookId")
    private RentingBookEntity book;

    @Column
    private int count;

    public Lead toLead() {
        return Lead.createLead(this.rentingId.getBookId(), 
                            this.rentingId.getUserId(), 
                            this.count, 
                            this.book.getTitle(), 
                            this.user.getNick());
    }

}
