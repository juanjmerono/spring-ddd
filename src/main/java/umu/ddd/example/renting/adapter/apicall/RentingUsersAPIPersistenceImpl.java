package umu.ddd.example.renting.adapter.apicall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import umu.ddd.example.renting.application.port.RentingUsersAPIPersistence;

@Service
public class RentingUsersAPIPersistenceImpl implements RentingUsersAPIPersistence {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${users.api.url}")
    private String apiBaseUrl;

    @Override
    public String getUserNick(String userId) throws Exception {
        String apiQuery = apiBaseUrl+"/"+userId;
        try {
            return restTemplate
                .getForEntity(apiQuery, APIUser.class)
                .getBody().getNickName();
        } catch (Exception nfe) {
            throw nfe;
        }
    }
    
}
