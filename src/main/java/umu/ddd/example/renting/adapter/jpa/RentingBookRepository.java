package umu.ddd.example.renting.adapter.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentingBookRepository extends PagingAndSortingRepository<RentingBookEntity,String>{
}
