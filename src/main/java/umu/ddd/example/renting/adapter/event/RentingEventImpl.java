package umu.ddd.example.renting.adapter.event;

import org.springframework.context.ApplicationEvent;

import umu.ddd.example.renting.domain.events.RentingEvent;
import umu.ddd.example.renting.domain.events.ReturnLeadEvent;
import umu.ddd.example.renting.domain.events.StartLeadEvent;
import umu.ddd.example.renting.domain.models.Lead;

public class RentingEventImpl extends ApplicationEvent {

    public RentingEventImpl(Object source) {
        super(source);
    }

    public RentingEvent getDomainEvent() {
        return (RentingEvent)this.getSource();
    }

    public Lead getLead() {
        return this.getDomainEvent().getLead();
    }

    public boolean isReturnLeadEvent() {
        return this.source instanceof ReturnLeadEvent;
    }

    public boolean isStartLeadEvent() {
        return this.source instanceof StartLeadEvent;
    }

}
