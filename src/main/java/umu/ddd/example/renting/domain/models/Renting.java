package umu.ddd.example.renting.domain.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Renting {
    private long count;
    private String next;
    private String previous;
    private List<Lead> leads;

    public static Renting createRenting(long count, String nxt, String prv, List<Lead> leads) {
        return Renting.builder()
                .count(count)
                .next(nxt)
                .previous(prv)
                .leads(leads)
                .build();       
    }
}
