package umu.ddd.example.renting.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lead {
    private String bookId;
    private String userId;
    private int count;
    private String title;
    private String nick;

    public static Lead createLead(String bookId, String userId, int count, String title, String nick) {
        return Lead.builder()
            .bookId(bookId)
            .userId(userId)
            .count(count)
            .title(title)
            .nick(nick)
            .build();
    }
}
