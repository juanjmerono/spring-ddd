package umu.ddd.example.renting.domain.events;

import umu.ddd.example.renting.domain.models.Lead;

public class StartLeadEvent extends RentingEvent {
    public StartLeadEvent(Lead lead) {
        super(lead);
    }
    public static StartLeadEvent createEvent(Lead l) {
        return new StartLeadEvent(l);
    }
}
