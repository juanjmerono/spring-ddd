package umu.ddd.example.renting.domain.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import umu.ddd.example.renting.domain.models.Lead;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RentingEvent {
    private Lead lead;    
}
