package umu.ddd.example.renting.domain.events;

import umu.ddd.example.renting.domain.models.Lead;

public class ReturnLeadEvent extends RentingEvent {
    public ReturnLeadEvent(Lead lead) {
        super(lead);
    }
    public static ReturnLeadEvent createEvent(Lead l) {
        return new ReturnLeadEvent(l);
    }
}
