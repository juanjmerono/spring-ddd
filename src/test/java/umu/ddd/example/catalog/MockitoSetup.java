package umu.ddd.example.catalog;

import java.util.Arrays;

import org.mockito.Mockito;

import umu.ddd.example.catalog.domain.models.Catalog;
import umu.ddd.example.catalog.domain.models.Book;
import umu.ddd.example.catalog.application.port.CatalogPersistence;
import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Author;

public class MockitoSetup {
    
    public static String [] BOOK_NAMES = new String[] {"Don Quijote de la Mancha","Novelas ejemplares","La galatea","Los trabajos de Persiles y Sigismunda"};
    public static int [] BOOK_IDS = new int[] {10,110,1110,11110};
    public static String AUTHOR_NAME = "Cervantes";
    public static int AUTHOR_BORN = 1547, AUTHOR_DIE = 1616;
    public static String NOT_FOUND_SEARCH = "notfound";
    public static String FOUND_SEARCH = "cervantes";
    public static String API_PATH = "/catalog/";
    public static String QUERY_STRING = API_PATH+"?page=%s";
    public static String QUERY_SEARCH_STRING = API_PATH+"?page=%s&search=%s";
    public static String API_SEARCH_PATH = "/catalog/?search=%s";

    private static Catalog buildCatalog(int size,String nxt, String prv, int bookId, String bookName) {
        return Catalog.builder()
            .count(size)
            .next(nxt)
            .previous(prv)
            .books(Arrays.asList(new Book[] { 
                Book.builder()
                    .id(bookId)
                    .title(bookName)
                    .authors(Arrays.asList(new Author[] { 
                        Author.builder()
                            .name(AUTHOR_NAME)
                            .born(AUTHOR_BORN)
                            .die(AUTHOR_DIE)
                            .build()}))
                    .build() }))
            .build();
    }

    public static void mockPersistence(CatalogPersistence catalogPersistence) {

        // Not found pages
        try {

            Mockito.when(catalogPersistence.loadBookById("174"))
                .thenReturn(Catalog.builder().count(1).books(Arrays.asList(new Book[]{Book.builder().id(174).title("174 Title").build()})).build());

            Mockito.when(catalogPersistence.loadBookById("000"))
                .thenThrow(BookNotFoundException.class);

            Mockito.when(catalogPersistence.loadPage(-1,null))
                .thenThrow(CatalogNotFoundException.class);
            Mockito.when(catalogPersistence.loadPage(1,NOT_FOUND_SEARCH))
                .thenThrow(CatalogNotFoundException.class);

            // No filter search
            Mockito.when(catalogPersistence.loadPage(1,null))
                .thenReturn(buildCatalog(4,String.format(QUERY_STRING,2),null,BOOK_IDS[0],BOOK_NAMES[0]));

            Mockito.when(catalogPersistence.loadPage(2,null))
                .thenReturn(buildCatalog(4,String.format(QUERY_STRING,3),API_PATH,BOOK_IDS[1],BOOK_NAMES[1]));

            Mockito.when(catalogPersistence.loadPage(3,null))
            .thenReturn(buildCatalog(4,String.format(QUERY_STRING,4),String.format(QUERY_STRING,2),BOOK_IDS[2],BOOK_NAMES[2]));

            Mockito.when(catalogPersistence.loadPage(4,null))
                .thenReturn(buildCatalog(4,null,String.format(QUERY_STRING,3),BOOK_IDS[3],BOOK_NAMES[3]));

            // Filtered search
            Mockito.when(catalogPersistence.loadPage(1,FOUND_SEARCH))
                .thenReturn(buildCatalog(4,String.format(QUERY_SEARCH_STRING,2,FOUND_SEARCH),null,BOOK_IDS[0],BOOK_NAMES[0]));

            Mockito.when(catalogPersistence.loadPage(2,FOUND_SEARCH))
                .thenReturn(buildCatalog(4,String.format(QUERY_SEARCH_STRING,3,FOUND_SEARCH),String.format(API_SEARCH_PATH,FOUND_SEARCH),BOOK_IDS[1],BOOK_NAMES[1]));

            Mockito.when(catalogPersistence.loadPage(3,FOUND_SEARCH))
                .thenReturn(buildCatalog(4,String.format(QUERY_SEARCH_STRING,4,FOUND_SEARCH),String.format(QUERY_SEARCH_STRING,2,FOUND_SEARCH),BOOK_IDS[2],BOOK_NAMES[2]));

            Mockito.when(catalogPersistence.loadPage(4,FOUND_SEARCH))
                .thenReturn(buildCatalog(4,null,String.format(QUERY_SEARCH_STRING,3,FOUND_SEARCH),BOOK_IDS[3],BOOK_NAMES[3]));

        } catch (Exception e) {
            e.printStackTrace();
        }
                    
    }

}
