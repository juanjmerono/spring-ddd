package umu.ddd.example.catalog.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import umu.ddd.example.catalog.CheckAsserts;
import umu.ddd.example.catalog.MockitoSetup;
import umu.ddd.example.catalog.application.port.CatalogPersistence;
import umu.ddd.example.catalog.application.usecases.GetCatalogUseCase;
import umu.ddd.example.catalog.domain.exceptions.BookNotFoundException;
import umu.ddd.example.catalog.domain.exceptions.CatalogNotFoundException;
import umu.ddd.example.catalog.domain.models.Catalog;
import umu.ddd.example.catalog.domain.models.Book;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class CatalogGetCatalogUseCaseTests {

    @Autowired
    private GetCatalogUseCase getCatalogUseCaseTest;

    @MockBean
    private CatalogPersistence catalogPersistence;

    @BeforeEach
    public void setup() {
        MockitoSetup.mockPersistence(catalogPersistence);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    public void getPageTest(int page) throws CatalogNotFoundException {
        Catalog catalog = getCatalogUseCaseTest.getPage(page,null);
        CheckAsserts.checkCatalogPage(page,catalog);
    }

    @Test
    public void notFoundPage() {
        assertThrows(CatalogNotFoundException.class, () -> getCatalogUseCaseTest.getPage(-1,null));
    }

    @Test
    public void notFoundSearch() {
        assertThrows(CatalogNotFoundException.class, () -> getCatalogUseCaseTest.getPage(1,MockitoSetup.NOT_FOUND_SEARCH));
    }

    @ParameterizedTest
    @CsvSource({"1,cervantes","2,cervantes","3,cervantes","4,cervantes"})
    public void getPageSearchTest(int page, String search) throws CatalogNotFoundException {
        Catalog catalog = getCatalogUseCaseTest.getPage(page,search);
        CheckAsserts.checkCatalogPageSearch(page,search,catalog);
    }

    @Test
    public void notFoundBook() {
        assertThrows(BookNotFoundException.class, () -> getCatalogUseCaseTest.getBookById("000"));
    }
    @Test
    public void foundBook() throws Exception {
        Book book = getCatalogUseCaseTest.getBookById("174");
        assertEquals(174, book.getId());
        assertEquals("174 Title", book.getTitle());
    }

}
