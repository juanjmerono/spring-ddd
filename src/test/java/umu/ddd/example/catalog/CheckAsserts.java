package umu.ddd.example.catalog;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;

import umu.ddd.example.catalog.domain.models.Catalog;

public class CheckAsserts {
    
    // Check Mock Catalog
    public static void checkCatalogPage(int page, Catalog catalog) {
        assertNotNull(catalog);
        assertEquals(4,catalog.getCount());
        if (page>1) {
            assertEquals((page==2)?MockitoSetup.API_PATH:String.format(MockitoSetup.QUERY_STRING,page-1),catalog.getPrevious());    
        } else {
            assertNull(catalog.getPrevious());
        }
        if (page<4) {
            assertEquals(String.format(MockitoSetup.QUERY_STRING,page+1),catalog.getNext());
        } else {
            assertNull(catalog.getNext());
        }
        assertNotNull(catalog.getBooks());
        assertEquals(1,catalog.getBooks().size());
        assertEquals(MockitoSetup.BOOK_IDS[page-1],catalog.getBooks().get(0).getId());
        assertEquals(MockitoSetup.BOOK_NAMES[page-1],catalog.getBooks().get(0).getTitle());
        assertEquals(1,catalog.getBooks().get(0).getAuthors().size());
        assertEquals(MockitoSetup.AUTHOR_NAME,catalog.getBooks().get(0).getAuthors().get(0).getName());
        assertEquals(MockitoSetup.AUTHOR_BORN,catalog.getBooks().get(0).getAuthors().get(0).getBorn());
        assertEquals(MockitoSetup.AUTHOR_DIE,catalog.getBooks().get(0).getAuthors().get(0).getDie());
    }

    public static void checkCatalogPageSearch(int page, String search, Catalog catalog) {
        assertNotNull(catalog);
        assertEquals(4,catalog.getCount());
        if (page>1) {
            assertEquals((page==2)?String.format(MockitoSetup.API_SEARCH_PATH,MockitoSetup.FOUND_SEARCH):String.format(MockitoSetup.QUERY_SEARCH_STRING,page-1,MockitoSetup.FOUND_SEARCH),catalog.getPrevious());    
        } else {
            assertNull(catalog.getPrevious());
        }
        if (page<4) {
            assertEquals(String.format(MockitoSetup.QUERY_SEARCH_STRING,page+1,MockitoSetup.FOUND_SEARCH),catalog.getNext());
        } else {
            assertNull(catalog.getNext());
        }
        assertNotNull(catalog.getBooks());
        assertEquals(1,catalog.getBooks().size());
        assertEquals(MockitoSetup.BOOK_IDS[page-1],catalog.getBooks().get(0).getId());
        assertEquals(MockitoSetup.BOOK_NAMES[page-1],catalog.getBooks().get(0).getTitle());
        assertEquals(1,catalog.getBooks().get(0).getAuthors().size());
        assertEquals(MockitoSetup.AUTHOR_NAME,catalog.getBooks().get(0).getAuthors().get(0).getName());
        assertEquals(MockitoSetup.AUTHOR_BORN,catalog.getBooks().get(0).getAuthors().get(0).getBorn());
        assertEquals(MockitoSetup.AUTHOR_DIE,catalog.getBooks().get(0).getAuthors().get(0).getDie());
    }
    
    private static String [] REAL_BOOK_NAMES = new String[] {"Pride and Prejudice", "The Scarlet Letter", "The Bondage and Travels of Johann Schiltberger, a Native of Bavaria, in Europe, Asia, and Africa, 1396-1427", "The Philistine: a periodical of protest (Vol. I, No. 2, July 1895)"};
    private static int [] REAL_BOOK_IDS = new int[] {1342, 25344, 52569, 68382};
    private static String [] REAL_AUTHOR_NAMES = new String[] {"Austen, Jane", "Hawthorne, Nathaniel", "Schiltberger, Johannes", "Various"};
    private static int [] REAL_BORNS = new int[] {1775, 1804, 1380, 0};
    private static int [] REAL_DIES = new int[] {1817, 1864, 0, 0};
    private static Integer [] TEST_PAGE = new Integer[] {1, 2, 150, 2135};
    private static String [] REAL_BOOK_NAMES_SEARCH = new String[] {"A Tale of Two Cities", "The Letters of Charles Dickens. Vol. 1, 1833-1856", "Speeches: Literary and Social", "Sanoma merellä"};
    private static int [] REAL_BOOK_IDS_SEARCH = new int[] {98, 25852, 824, 31638};
    private static int SEARCH_BORN = 1812, SEARCH_DIE = 1870;
    private static String SEARCH_AUTHOR_NAME = "Dickens, Charles";
    private static Integer [] TEST_PAGE_SEARCH = new Integer[] {1, 2, 3, 7};

    // Check Real Catalog
    public static void checkRealCatalogPage(int page, Catalog catalog) {
        assertNotNull(catalog);
        assertEquals(68305,catalog.getCount());
        int indexPage = Arrays.asList(TEST_PAGE).indexOf(page);
        int pageSize = 32;
        if (page>1) {
            assertEquals((page==2)?MockitoSetup.API_PATH:String.format(MockitoSetup.QUERY_STRING,page-1),catalog.getPrevious());    
        } else {
            assertNull(catalog.getPrevious());
        }
        if (page<2134) {
            assertEquals(String.format(MockitoSetup.QUERY_STRING,page+1),catalog.getNext());
        } else {
            assertNull(catalog.getNext());
            pageSize = 17;
        }
        assertNotNull(catalog.getBooks());
        assertEquals(pageSize,catalog.getBooks().size());
        assertEquals(REAL_BOOK_IDS[indexPage],catalog.getBooks().get(0).getId());
        assertEquals(REAL_BOOK_NAMES[indexPage],catalog.getBooks().get(0).getTitle());
        assertEquals(1,catalog.getBooks().get(0).getAuthors().size());
        assertEquals(REAL_AUTHOR_NAMES[indexPage],catalog.getBooks().get(0).getAuthors().get(0).getName());
        assertEquals(REAL_BORNS[indexPage],catalog.getBooks().get(0).getAuthors().get(0).getBorn());
        assertEquals(REAL_DIES[indexPage],catalog.getBooks().get(0).getAuthors().get(0).getDie());
    }

    public static void checkRealCatalogPageSearch(int page, String search, Catalog catalog) {
        assertNotNull(catalog);
        assertEquals(218,catalog.getCount());
        int indexPage = Arrays.asList(TEST_PAGE_SEARCH).indexOf(page);
        int pageSize = 32;
        if (page>1) {
            assertEquals((page==2)?String.format(MockitoSetup.API_SEARCH_PATH,search):String.format(MockitoSetup.QUERY_SEARCH_STRING,page-1,search),catalog.getPrevious());    
        } else {
            assertNull(catalog.getPrevious());
        }
        if (page<7) {
            assertEquals(String.format(MockitoSetup.QUERY_SEARCH_STRING,page+1,search),catalog.getNext());
        } else {
            assertNull(catalog.getNext());
            pageSize = 26;
        }
        assertNotNull(catalog.getBooks());
        assertEquals(pageSize,catalog.getBooks().size());
        assertEquals(REAL_BOOK_IDS_SEARCH[indexPage],catalog.getBooks().get(0).getId());
        assertEquals(REAL_BOOK_NAMES_SEARCH[indexPage],catalog.getBooks().get(0).getTitle());
        assertEquals(1,catalog.getBooks().get(0).getAuthors().size());
        assertEquals(SEARCH_AUTHOR_NAME,catalog.getBooks().get(0).getAuthors().get(0).getName());
        assertEquals(SEARCH_BORN,catalog.getBooks().get(0).getAuthors().get(0).getBorn());
        assertEquals(SEARCH_DIE,catalog.getBooks().get(0).getAuthors().get(0).getDie());
    }

}
