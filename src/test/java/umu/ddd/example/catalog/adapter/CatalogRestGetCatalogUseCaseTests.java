package umu.ddd.example.catalog.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.catalog.domain.models.Book;
import umu.ddd.example.catalog.domain.models.Catalog;
import umu.ddd.example.catalog.CheckAsserts;
import umu.ddd.example.catalog.MockitoSetup;
import umu.ddd.example.catalog.application.port.CatalogPersistence;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class CatalogRestGetCatalogUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CatalogPersistence catalogPersistence;

    @BeforeEach
    public void setup() {
        MockitoSetup.mockPersistence(catalogPersistence);
    }

    @Test
    public void searchDefaultPage() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Catalog catalog = objectMapper.readValue(content, Catalog.class);
        CheckAsserts.checkCatalogPage(1,catalog);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    public void getPageTest(int page) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/?page="+page)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Catalog catalog = objectMapper.readValue(content, Catalog.class);
        CheckAsserts.checkCatalogPage(page,catalog);
    }

    @Test
    public void notFoundPage() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/?page=-1")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void notFoundSearch() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/?page=1&search=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @ParameterizedTest
    @CsvSource({"1,cervantes","2,cervantes","3,cervantes","4,cervantes"})
    public void getPageSearchTest(int page, String search) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format(MockitoSetup.QUERY_SEARCH_STRING,page,search))
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Catalog catalog = objectMapper.readValue(content, Catalog.class);
        CheckAsserts.checkCatalogPageSearch(page,search,catalog);
    }


    @Test
    public void getByIdNotFound() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/000")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void getByIdOk() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/catalog/174")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Book book = objectMapper.readValue(content, Book.class);
        assertEquals(174, book.getId());
        assertEquals("174 Title", book.getTitle());
    }


}
