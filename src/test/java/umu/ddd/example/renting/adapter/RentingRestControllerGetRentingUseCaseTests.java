package umu.ddd.example.renting.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.renting.domain.models.Renting;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class RentingRestControllerGetRentingUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @ParameterizedTest
    @ValueSource(ints={0,1,2,3})
    public void testFindAll(int page) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/renting/?page="+page)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Renting renting = objectMapper.readValue(content, Renting.class);
        assertNotNull(renting);
        assertEquals(40,renting.getCount());
        if (page>0) {
            assertEquals("/renting/?page="+(page-1),renting.getPrevious());
        } else {
            assertNull(renting.getPrevious());
        }
        if (page<3) {
            assertEquals("/renting/?page="+(page+1),renting.getNext());
        } else {
            assertNull(renting.getNext());
        }
        assertNotNull(renting.getLeads());
        assertEquals(10,renting.getLeads().size());
        assertEquals("id-0"+page+"0",renting.getLeads().get(0).getBookId());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+page+"0",renting.getLeads().get(0).getUserId());
        assertEquals(1,renting.getLeads().get(0).getCount());
    }

    @Test
    public void testNotFoundPage() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/renting/?page=-1")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void testNotFoundSearch() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/renting/?page=0&search=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @ParameterizedTest
    @CsvSource({"0,it","1,it"})
    public void testFindFiltered(int page, String search) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/renting/?page="+page+"&search="+search)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Renting renting = objectMapper.readValue(content, Renting.class);
        assertNotNull(renting);
        assertEquals(20,renting.getCount());
        if (page>0) {
            assertEquals("/renting/?page="+(page-1)+"&search="+search,renting.getPrevious());
        } else {
            assertNull(renting.getPrevious());
        }
        if (page<1) {
            assertEquals("/renting/?page="+(page+1)+"&search="+search,renting.getNext());
        } else {
            assertNull(renting.getNext());
        }
        assertNotNull(renting.getLeads());
        assertEquals(10,renting.getLeads().size());
        assertEquals("it-0"+(page*2)+"1",renting.getLeads().get(0).getBookId());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+(page*2)+"1",renting.getLeads().get(0).getUserId());
        assertEquals(1,renting.getLeads().get(0).getCount());
    }

}
