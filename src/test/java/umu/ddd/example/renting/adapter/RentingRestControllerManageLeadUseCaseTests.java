package umu.ddd.example.renting.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.renting.adapter.event.RentingEventImpl;
import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.application.port.RentingStockAPIPersistence;
import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.exceptions.RentingStockException;
import umu.ddd.example.renting.domain.models.Lead;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
@AutoConfigureMockMvc
public class RentingRestControllerManageLeadUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RentingPersistence rentingPersistence;

    @MockBean
    private RentingStockAPIPersistence stockPersistence;

    @Autowired 
    private ApplicationEvents applicationEvents;

    @BeforeEach
    public void setup() throws RentingStockException, DuplicateRentingException, LimitRentingException, RentingNotFoundException {
        Mockito.when(rentingPersistence.startLead("id-0","uid-0",2))
            .thenReturn(Lead.builder().bookId("id-0").userId("uid-0").count(1).build());
        Mockito.when(rentingPersistence.startLead("id-1","uid-1",2))
            .thenReturn(Lead.builder().bookId("id-1").userId("uid-1").count(1).build());
        Mockito.when(rentingPersistence.startLead("001","001",2))
            .thenThrow(DuplicateRentingException.class);
        Mockito.when(rentingPersistence.startLead("002","002",2))
            .thenThrow(LimitRentingException.class);
        Mockito.when(rentingPersistence.returnLead("id-0","uid-0"))
            .thenReturn(Lead.builder().bookId("id-0").userId("uid-0").count(0).build());
        Mockito.when(rentingPersistence.returnLead("id-1","uid-1"))
            .thenReturn(Lead.builder().bookId("id-1").userId("uid-1").count(0).build());
        Mockito.when(rentingPersistence.returnLead("000","000"))
            .thenThrow(RentingNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("id-0"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("001"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("002"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("000"))
            .thenReturn(true);
    }

    @ParameterizedTest
    @CsvSource({"id-0,uid-0","id-1,uid-1"})
    public void testStartLead(String bookId, String userId) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/renting/loan/?bookid="+bookId+"&userid="+userId)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Lead lead = objectMapper.readValue(content, Lead.class);
        assertNotNull(lead);
        assertEquals(bookId,lead.getBookId());
        assertEquals(userId,lead.getUserId());
        assertEquals(1,lead.getCount());
        assertEquals(1, applicationEvents
                .stream(RentingEventImpl.class)
                .filter(event -> event.isStartLeadEvent() 
                                && event.getLead().getBookId().equals(bookId)
                                && event.getLead().getUserId().equals(userId) 
                                && event.getLead().getCount() == 1)
                .count());
    }

    @ParameterizedTest
    @CsvSource({"id-0,uid-0","id-1,uid-1"})
    public void testReturnLead(String bookId, String userId) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/renting/return/?bookid="+bookId+"&userid="+userId)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Lead lead = objectMapper.readValue(content, Lead.class);
        assertNotNull(lead);
        assertEquals(bookId,lead.getBookId());
        assertEquals(userId,lead.getUserId());
        assertEquals(0,lead.getCount());
        assertEquals(1, applicationEvents
                .stream(RentingEventImpl.class)
                .filter(event -> event.isReturnLeadEvent()
                                && event.getLead().getBookId().equals(bookId)
                                && event.getLead().getUserId().equals(userId) 
                                && event.getLead().getCount() == 0)
                .count());
    }

    @Test
    public void testReturnLeadNoBook() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/renting/return/?bookid=000&userid=000")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }
    
}
