package umu.ddd.example.renting.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;

import umu.ddd.example.renting.adapter.event.RentingEventImpl;
import umu.ddd.example.renting.application.port.RentingPersistence;
import umu.ddd.example.renting.application.port.RentingStockAPIPersistence;
import umu.ddd.example.renting.application.usecases.ManageLeadUseCase;
import umu.ddd.example.renting.domain.exceptions.DuplicateRentingException;
import umu.ddd.example.renting.domain.exceptions.LimitRentingException;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.exceptions.RentingStockException;
import umu.ddd.example.renting.domain.models.Lead;


@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
public class RentingManageLeadUseCaseTests {
    
    @Autowired
    private ManageLeadUseCase managLeadUseCaseTest;

    @MockBean
    private RentingPersistence rentingPersistence;

    @MockBean
    private RentingStockAPIPersistence stockPersistence;

    @Autowired 
    private ApplicationEvents applicationEvents;

    @BeforeEach
    public void setup() throws RentingStockException, DuplicateRentingException, LimitRentingException, RentingNotFoundException {
        Mockito.when(rentingPersistence.startLead("id-0","uid-0",2))
            .thenReturn(Lead.builder().bookId("id-0").userId("uid-0").count(1).build());
        Mockito.when(rentingPersistence.startLead("id-1","uid-1",2))
            .thenReturn(Lead.builder().bookId("id-1").userId("uid-1").count(1).build());
        Mockito.when(rentingPersistence.startLead("001","001",2))
            .thenThrow(DuplicateRentingException.class);
        Mockito.when(rentingPersistence.startLead("002","002",2))
            .thenThrow(LimitRentingException.class);
        Mockito.when(rentingPersistence.returnLead("id-0","uid-0"))
            .thenReturn(Lead.builder().bookId("id-0").userId("uid-0").count(0).build());
        Mockito.when(rentingPersistence.returnLead("id-1","uid-1"))
            .thenReturn(Lead.builder().bookId("id-1").userId("uid-1").count(0).build());
        Mockito.when(rentingPersistence.returnLead("000","000"))
            .thenThrow(RentingNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("id-0"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("001"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("002"))
            .thenReturn(true);
        Mockito.when(stockPersistence.decreaseStock("000"))
            .thenReturn(true);
    }

    @Test
    public void testStartLead() throws DuplicateRentingException, LimitRentingException, RentingStockException {
        Lead lead = managLeadUseCaseTest.startLead("id-0", "uid-0");
        assertEquals("id-0",lead.getBookId());
        assertEquals("uid-0",lead.getUserId());
        assertEquals(1,lead.getCount());
        assertEquals(1, applicationEvents
                .stream(RentingEventImpl.class)
                .filter(event -> event.isStartLeadEvent()
                                && event.getLead().getBookId().equals("id-0")
                                && event.getLead().getUserId().equals("uid-0") 
                                && event.getLead().getCount() == 1)
                .count());
    }

    @Test
    public void testStartDuplicateLead() throws DuplicateRentingException, LimitRentingException {
        assertThrows(DuplicateRentingException.class, () -> managLeadUseCaseTest.startLead("001","001"));
    }

    @Test
    public void testStartLimitedLead() throws DuplicateRentingException, LimitRentingException {
        assertThrows(LimitRentingException.class, () -> managLeadUseCaseTest.startLead("002","002"));
    }

    @Test
    public void testReturnLead() throws RentingNotFoundException, RentingStockException {
        Lead lead = managLeadUseCaseTest.returnLead("id-0", "uid-0");
        assertEquals("id-0",lead.getBookId());
        assertEquals("uid-0",lead.getUserId());
        assertEquals(0,lead.getCount());
        assertEquals(1, applicationEvents
                .stream(RentingEventImpl.class)
                .filter(event -> event.isReturnLeadEvent()
                                && event.getLead().getBookId().equals("id-0")
                                && event.getLead().getUserId().equals("uid-0") 
                                && event.getLead().getCount() == 0)
                .count());
    }

    @Test
    public void testReturnLeadNotFound() {
        assertThrows(RentingNotFoundException.class, () -> managLeadUseCaseTest.returnLead("000","000"));
    }

}
