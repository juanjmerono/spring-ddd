package umu.ddd.example.renting.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.RecordApplicationEvents;

import umu.ddd.example.renting.application.usecases.GetRentingUseCase;
import umu.ddd.example.renting.domain.exceptions.RentingNotFoundException;
import umu.ddd.example.renting.domain.models.Renting;


@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
public class RentingGetRentingUseCaseTests {
    
    @Autowired
    private GetRentingUseCase getRentingUseCaseTest;

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void testFindAll(int page) throws RentingNotFoundException {
        Renting renting = getRentingUseCaseTest.getPage(page,null);
        assertNotNull(renting);
        assertEquals(40,renting.getCount());
        if (page>0) {
            assertEquals("/renting/?page="+(page-1),renting.getPrevious());
        } else {
            assertNull(renting.getPrevious());
        }
        if (page<3) {
            assertEquals("/renting/?page="+(page+1),renting.getNext());
        } else {
            assertNull(renting.getNext());
        }
        assertNotNull(renting.getLeads());
        assertEquals(10,renting.getLeads().size());
        assertEquals("id-0"+page+"0",renting.getLeads().get(0).getBookId());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+page+"0",renting.getLeads().get(0).getUserId());
        assertEquals(1,renting.getLeads().get(0).getCount());
    }

    @Test
    public void testNotFoundPage() {
        assertThrows(RentingNotFoundException.class, () -> getRentingUseCaseTest.getPage(-1,null));
    }

    @Test
    public void testNotFoundSearch() {
        assertThrows(RentingNotFoundException.class, () -> getRentingUseCaseTest.getPage(0,"notfound"));
    }

    @ParameterizedTest
    @CsvSource({"0,it","1,it"})
    public void testFindFiltered(int page, String search) throws RentingNotFoundException {
        Renting renting = getRentingUseCaseTest.getPage(page,search);
        assertNotNull(renting);
        assertEquals(20,renting.getCount());
        if (page>0) {
            assertEquals("/renting/?page="+(page-1)+"&search="+search,renting.getPrevious());
        } else {
            assertNull(renting.getPrevious());
        }
        if (page<1) {
            assertEquals("/renting/?page="+(page+1)+"&search="+search,renting.getNext());
        } else {
            assertNull(renting.getNext());
        }
        assertNotNull(renting.getLeads());
        assertEquals(10,renting.getLeads().size());
        assertEquals("it-0"+(page*2)+"1",renting.getLeads().get(0).getBookId());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+(page*2)+"1",renting.getLeads().get(0).getUserId());
        assertEquals(1,renting.getLeads().get(0).getCount());
    }

}
