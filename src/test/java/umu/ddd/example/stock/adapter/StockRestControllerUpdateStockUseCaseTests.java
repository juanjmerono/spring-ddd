package umu.ddd.example.stock.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.stock.adapter.event.StockEventImpl;
import umu.ddd.example.stock.application.port.StockCatalogAPIPersistence;
import umu.ddd.example.stock.application.port.StockPersistence;
import umu.ddd.example.stock.domain.exceptions.BookNotFoundException;
import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
@AutoConfigureMockMvc
public class StockRestControllerUpdateStockUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StockPersistence stockPersistence;

    @MockBean
    private StockCatalogAPIPersistence bookPersistence;

    @Autowired 
    private ApplicationEvents applicationEvents;
    
    @BeforeEach
    public void setup() throws BookNotFoundException, StockNotFoundException, LimitStockException {
        Mockito.when(stockPersistence.updateStock("id-0", 10))
            .thenReturn(Book.builder()
                        .bookId("id-0")
                        .count(10).build());       
        Mockito.when(stockPersistence.updateStock("id-1", 11))
            .thenReturn(Book.builder()
                        .bookId("id-1")
                        .count(11).build());
        Mockito.when(bookPersistence.getBookTitle("id-0"))
            .thenReturn("mi test title");
        Mockito.when(bookPersistence.getBookTitle("id-1"))
            .thenReturn("mi test title");
        Mockito.when(stockPersistence.updateBookTitle("id-0","mi test title"))
            .thenReturn(Book.builder().bookId("id-0").title("mi test title").count(0).build());
        Mockito.when(stockPersistence.updateBookTitle("id-1","mi test title"))
            .thenReturn(Book.builder().bookId("id-1").title("mi test title").count(0).build());

        Mockito.when(stockPersistence.updateBookTitle("notfound",null))
            .thenThrow(StockNotFoundException.class);

        Mockito.when(stockPersistence.increaseStock("notfound"))
            .thenThrow(StockNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("notfound"))
            .thenThrow(StockNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("cerobook"))
            .thenThrow(LimitStockException.class);
    }

    @ParameterizedTest
    @CsvSource({"id-0,10","id-1,11"})
    public void testChangeStock(String bookId, int count) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/stock/update/?book="+bookId+"&count="+count)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Book book = objectMapper.readValue(content, Book.class);
        assertEquals(1, applicationEvents
                .stream(StockEventImpl.class)
                .filter(event -> event.isResetStockEvent()
                            && event.getBook().getBookId().equals(bookId) 
                            && event.getBook().getCount() == count)
                .count());
        assertNotNull(book);
        assertEquals(bookId,book.getBookId());
        assertEquals(count,book.getCount());
    }

    @Test
    public void testChangeStockNoData() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/stock/update/")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @ParameterizedTest
    @ValueSource(ints={0,-1})
    public void testChangeInvalidStock(int stockValue) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/stock/update/?book=id-0&count="+stockValue)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void testIncreaseStockNoData() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/stock/increase/")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void testIncreaseStockNoBook() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/stock/increase/?bookid=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void testDecreaseStockNoData() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/stock/decrease/")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void testDecreaseStockNoBook() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/stock/decrease/?bookid=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void testDecreaseStockLimitBook() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/stock/decrease/?bookid=cerobook")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

}
