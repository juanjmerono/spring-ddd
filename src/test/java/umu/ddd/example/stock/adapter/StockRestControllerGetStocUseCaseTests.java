package umu.ddd.example.stock.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.stock.application.port.StockCatalogAPIPersistence;
import umu.ddd.example.stock.domain.models.Stock;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class StockRestControllerGetStocUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StockCatalogAPIPersistence bookPersistence;

    @ParameterizedTest
    @ValueSource(ints={0,1,2,3})
    public void testFindAll(int page) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/stock/?page="+page)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Stock stock = objectMapper.readValue(content, Stock.class);
        assertNotNull(stock);
        assertEquals(40,stock.getCount());
        if (page>0) {
            assertEquals("/stock/?page="+(page-1),stock.getPrevious());
        } else {
            assertNull(stock.getPrevious());
        }
        if (page<3) {
            assertEquals("/stock/?page="+(page+1),stock.getNext());
        } else {
            assertNull(stock.getNext());
        }
        assertNotNull(stock.getBooks());
        assertEquals(10,stock.getBooks().size());
        assertEquals("id-0"+page+"0",stock.getBooks().get(0).getBookId());
        assertEquals(page*10,stock.getBooks().get(0).getCount());
    }

    @Test
    public void testNotFoundPage() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/stock/?page=-1")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void testNotFoundSearch() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/stock/?page=0&search=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @ParameterizedTest
    @CsvSource({"0,it-0"})
    public void testFindAllFiltered(int page, String search) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format("/stock/?page=%s&search=%s",page,search))
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Stock stock = objectMapper.readValue(content, Stock.class);
        assertNotNull(stock);
        assertEquals(20,stock.getCount());
        if (page>0) {
            assertEquals("/stock/?page="+(page-1)+"&search="+search,stock.getPrevious());
        } else {
            assertNull(stock.getPrevious());
        }
        if (page<1) {
            assertEquals("/stock/?page="+(page+1)+"&search="+search,stock.getNext());
        } else {
            assertNull(stock.getNext());
        }
        assertNotNull(stock.getBooks());
        assertEquals(10,stock.getBooks().size());
        assertEquals("it-0"+(page*2)+"1",stock.getBooks().get(0).getBookId());
        assertEquals((page*20)+1,stock.getBooks().get(0).getCount());
    }

}
