package umu.ddd.example.stock.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;

import umu.ddd.example.stock.adapter.event.StockEventImpl;
import umu.ddd.example.stock.application.port.StockCatalogAPIPersistence;
import umu.ddd.example.stock.application.port.StockPersistence;
import umu.ddd.example.stock.application.usecases.UpdateStockUseCase;
import umu.ddd.example.stock.domain.exceptions.BookNotFoundException;
import umu.ddd.example.stock.domain.exceptions.InvalidStockException;
import umu.ddd.example.stock.domain.exceptions.LimitStockException;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Book;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
public class StockUpdateStockUseCaseTests {
    
    @Autowired
    private UpdateStockUseCase updateStockUseCaseTest;

    @MockBean
    private StockPersistence stockPersistence;

    @MockBean
    private StockCatalogAPIPersistence bookPersistence;

    @Autowired 
    private ApplicationEvents applicationEvents;

    @BeforeEach
    public void setup() throws BookNotFoundException, StockNotFoundException, LimitStockException {
        Mockito.when(stockPersistence.updateStock("id-0", 10))
            .thenReturn(Book.builder()
                        .bookId("id-0")
                        .count(10).build());       
        Mockito.when(stockPersistence.updateStock("id-1", 11))
            .thenReturn(Book.builder()
                        .bookId("id-1")
                        .count(11).build());
        Mockito.when(bookPersistence.getBookTitle("id-0"))
            .thenReturn("mi test title");
        Mockito.when(bookPersistence.getBookTitle("id-1"))
            .thenReturn("mi test title");
        Mockito.when(stockPersistence.updateBookTitle("id-0","mi test title"))
            .thenReturn(Book.builder().bookId("id-0").title("mi test title").count(0).build());
        Mockito.when(stockPersistence.updateBookTitle("id-1","mi test title"))
            .thenReturn(Book.builder().bookId("id-1").title("mi test title").count(0).build());

        Mockito.when(stockPersistence.updateBookTitle("notfound",null))
            .thenThrow(StockNotFoundException.class);

        Mockito.when(stockPersistence.increaseStock("notfound"))
            .thenThrow(StockNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("notfound"))
            .thenThrow(StockNotFoundException.class);
        Mockito.when(stockPersistence.decreaseStock("cerobook"))
            .thenThrow(LimitStockException.class);
    }

    @Test
    public void testChangeStock() throws InvalidStockException {
        Book book = updateStockUseCaseTest.changeStock("id-0", 10);
        assertEquals("id-0",book.getBookId());
        assertEquals(10,book.getCount());
        assertEquals(1, applicationEvents
                .stream(StockEventImpl.class)
                .filter(event -> event.isResetStockEvent()
                            && event.getBook().getBookId().equals("id-0") 
                            && event.getBook().getCount() == 10)
                .count());
    }

    @ParameterizedTest
    @ValueSource(ints={0,-1})
    public void testChangeInvalidStock(int stockValue) {
        assertThrows(InvalidStockException.class, () -> updateStockUseCaseTest.changeStock("id-0", stockValue));
    }

    @Test
    public void testUpdateNoBookTitle() {
        assertThrows(StockNotFoundException.class, () -> updateStockUseCaseTest.updateBookTitle("notfound"));
    }

    @Test
    public void testUpdateBookTitle() throws StockNotFoundException {
        updateStockUseCaseTest.updateBookTitle("id-0");
        assertEquals(1, applicationEvents
                .stream(StockEventImpl.class)
                .filter(event -> event.isUpdateStockTitleEvent()
                            && event.getBook().getBookId().equals("id-0") 
                            && event.getBook().getTitle().equals("mi test title"))
                .count());
    }

    @Test
    public void testIncreaseStockNoBook() {
        assertThrows(StockNotFoundException.class, () -> updateStockUseCaseTest.increaseStock("notfound"));
    }

    @Test
    public void testDecreaseStockNoBook() {
        assertThrows(StockNotFoundException.class, () -> updateStockUseCaseTest.decreaseStock("notfound"));
    }

    @Test
    public void testDecreaseStockLimitBook() {
        assertThrows(LimitStockException.class, () -> updateStockUseCaseTest.decreaseStock("cerobook"));
    }

}
