package umu.ddd.example.stock.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import umu.ddd.example.stock.application.usecases.GetStockUseCase;
import umu.ddd.example.stock.domain.exceptions.StockNotFoundException;
import umu.ddd.example.stock.domain.models.Stock;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class StockGetStockUseCaseTests {
    
    @Autowired
    private GetStockUseCase getStockUseCaseTest;

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void testFindAll(int page) throws StockNotFoundException {
        Stock stock = getStockUseCaseTest.getPage(page,null);
        assertNotNull(stock);
        assertEquals(40,stock.getCount());
        if (page>0) {
            assertEquals("/stock/?page="+(page-1),stock.getPrevious());
        } else {
            assertNull(stock.getPrevious());
        }
        if (page<3) {
            assertEquals("/stock/?page="+(page+1),stock.getNext());
        } else {
            assertNull(stock.getNext());
        }
        assertNotNull(stock.getBooks());
        assertEquals(10,stock.getBooks().size());
        assertEquals("id-0"+page+"0",stock.getBooks().get(0).getBookId());
        assertEquals(page*10,stock.getBooks().get(0).getCount());
    }

    @Test
    public void testNotFoundPage() {
        assertThrows(StockNotFoundException.class, () -> getStockUseCaseTest.getPage(-1,null));
    }

    @Test
    public void testNotFoundSearch() {
        assertThrows(StockNotFoundException.class, () -> getStockUseCaseTest.getPage(0,"notfound"));
    }

    @ParameterizedTest
    @CsvSource({"0,it","1,it"})
    public void testFindAllFiltered(int page, String search) throws StockNotFoundException {
        Stock stock = getStockUseCaseTest.getPage(page,search);
        assertNotNull(stock);
        assertEquals(20,stock.getCount());
        if (page>0) {
            assertEquals("/stock/?page="+(page-1)+"&search="+search,stock.getPrevious());
        } else {
            assertNull(stock.getPrevious());
        }
        if (page<1) {
            assertEquals("/stock/?page="+(page+1)+"&search="+search,stock.getNext());
        } else {
            assertNull(stock.getNext());
        }
        assertNotNull(stock.getBooks());
        assertEquals(10,stock.getBooks().size());
        assertEquals("it-0"+(page*2)+"1",stock.getBooks().get(0).getBookId());
        assertEquals((page*20)+1,stock.getBooks().get(0).getCount());
    }

}
