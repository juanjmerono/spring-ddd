package umu.ddd.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class DddExampleWebControllerTests {
    
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @ValueSource(strings={"/web/", "/web/catalog/", "/web/users/", "/web/stock/", "/web/renting/"})
    void testIndexPage(String url) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        assertEquals("index.html",mvcResult.getModelAndView().getViewName());
    }

}
