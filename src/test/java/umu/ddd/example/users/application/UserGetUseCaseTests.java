package umu.ddd.example.users.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import umu.ddd.example.users.application.usecase.GetUsersUseCase;
import umu.ddd.example.users.domain.exceptions.MembersNotFoundException;
import umu.ddd.example.users.domain.exceptions.UserNotFoundException;
import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class UserGetUseCaseTests {
    
    @Autowired
    private GetUsersUseCase getUserUseCaseTest;

    @ParameterizedTest
    @ValueSource(ints={0,1,2,3})
    public void testFindAll(int page) throws Exception {
        Members members = getUserUseCaseTest.getPage(page, null);
        assertNotNull(members);
        assertEquals(40,members.getCount());
        if (page>0) {
            assertEquals("/users/?page="+(page-1),members.getPrevious());
        } else {
            assertNull(members.getPrevious());
        }
        if (page<3) {
            assertEquals("/users/?page="+(page+1),members.getNext());
        } else {
            assertNull(members.getNext());
        }
        assertNotNull(members.getUsers());
        assertEquals(10,members.getUsers().size());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+page+"0",members.getUsers().get(0).getId());
        assertEquals("nickname0"+page+"0",members.getUsers().get(0).getNickName());
    }

    @Test
    public void testNotFoundPage() {
        assertThrows(MembersNotFoundException.class, () -> getUserUseCaseTest.getPage(-1,null));
    }

    @Test
    public void testNotFoundSearch() {
        assertThrows(MembersNotFoundException.class, () -> getUserUseCaseTest.getPage(0,"notfound"));
    }

    @ParameterizedTest
    @CsvSource({"0,nicknama","1,nicknama"})
    public void testFindAllFiltered(int page, String search) throws MembersNotFoundException {
        Members members = getUserUseCaseTest.getPage(page,search);
        assertNotNull(members);
        assertEquals(20,members.getCount());
        if (page>0) {
            assertEquals("/users/?page="+(page-1)+"&search="+search,members.getPrevious());
        } else {
            assertNull(members.getPrevious());
        }
        if (page<1) {
            assertEquals("/users/?page="+(page+1)+"&search="+search,members.getNext());
        } else {
            assertNull(members.getNext());
        }
        assertNotNull(members.getUsers());
        assertEquals(10,members.getUsers().size());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+(page*2)+"1",members.getUsers().get(0).getId());
        assertEquals("nicknama0"+(page*2)+"1",members.getUsers().get(0).getNickName());
    }

    @Test
    public void testFindOnlyOneFiltered() throws MembersNotFoundException {
        Members members = getUserUseCaseTest.getPage(0,"nickname004");
        assertNotNull(members);
        assertEquals(1,members.getCount());
        assertNull(members.getPrevious());
        assertNull(members.getNext());
        assertNotNull(members.getUsers());
        assertEquals(1,members.getUsers().size());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea004",members.getUsers().get(0).getId());
        assertEquals("nickname004",members.getUsers().get(0).getNickName());
    }


    @Test
    public void testFindUserById() throws UserNotFoundException {
        User usr = getUserUseCaseTest.getUserById("2c65ebf7-964a-47c2-8807-a871489ea002");
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea002", usr.getId());
        assertEquals("nickname002", usr.getNickName());
    }


}
