package umu.ddd.example.users.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;

import umu.ddd.example.users.adapter.event.UserEventImpl;
import umu.ddd.example.users.application.port.UserPersistence;
import umu.ddd.example.users.application.usecase.RegisterUserUseCase;
import umu.ddd.example.users.domain.exceptions.InvalidNickNameException;
import umu.ddd.example.users.domain.models.User;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
public class UserRegisterUseCaseTests {
    
    @Autowired
    private RegisterUserUseCase registerUserUseCaseTest;

    @Autowired 
    private ApplicationEvents applicationEvents;

    @MockBean
    private UserPersistence userPersistence;

    @BeforeEach
    public void setup() {
        Mockito.when(userPersistence.findByNickName("nickname000"))
            .thenReturn(User.builder().id("2c65ebf7-964a-47c2-8807-a871489ea000").nickName("nickname000").build());
    }

    @ParameterizedTest
    @ValueSource(strings={"nickname000","newuser1"})
    public void testRegisterUser(String testNick) throws InvalidNickNameException {
        User usr = registerUserUseCaseTest.registerUser(testNick);
        assertNotNull(usr);
        if ("newuser1".equals(testNick)) {
            assertEquals(36,usr.getId().length());
        } else {
            assertEquals("2c65ebf7-964a-47c2-8807-a871489ea000",usr.getId());
        }
        assertEquals(testNick,usr.getNickName());
        assertEquals("newuser1".equals(testNick)?1:0, applicationEvents
                .stream(UserEventImpl.class)
                .filter(event -> event.isNewUserEvent() && event.getUser().getNickName().equals(testNick))
                .count());
    }

    @ParameterizedTest
    @ValueSource(strings={"p","ppppp"})
    public void testRegisterInvalidUser(String testNick) throws InvalidNickNameException {
        assertThrows(InvalidNickNameException.class, () -> registerUserUseCaseTest.registerUser(testNick));
    }

    @Test
    public void testRegisterNullUser() throws InvalidNickNameException {
        assertThrows(InvalidNickNameException.class, () -> registerUserUseCaseTest.registerUser(null));
    }

}
