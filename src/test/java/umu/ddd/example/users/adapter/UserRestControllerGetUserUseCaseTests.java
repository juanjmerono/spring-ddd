package umu.ddd.example.users.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.users.domain.models.Members;
import umu.ddd.example.users.domain.models.User;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class UserRestControllerGetUserUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @ParameterizedTest
    @ValueSource(ints={0,1,2,3})
    public void testFindAll(int page) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/?page="+page)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Members members = objectMapper.readValue(content, Members.class);
        assertNotNull(members);
        assertEquals(40,members.getCount());
        if (page>0) {
            assertEquals("/users/?page="+(page-1),members.getPrevious());
        } else {
            assertNull(members.getPrevious());
        }
        if (page<3) {
            assertEquals("/users/?page="+(page+1),members.getNext());
        } else {
            assertNull(members.getNext());
        }
        assertNotNull(members.getUsers());
        assertEquals(10,members.getUsers().size());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+page+"0",members.getUsers().get(0).getId());
        assertEquals("nickname0"+page+"0",members.getUsers().get(0).getNickName());
    }

    @Test
    public void notFoundPage() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/?page=-1")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void notFoundSearch() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/?page=0&search=notfound")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @ParameterizedTest
    @CsvSource({"0,nicknama","1,nicknama"})
    public void getPageSearchTest(int page, String search) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(String.format("/users/?page=%s&search=%s",page,search))
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Members members = objectMapper.readValue(content, Members.class);
        assertNotNull(members);
        assertEquals(20,members.getCount());
        if (page>0) {
            assertEquals("/users/?page="+(page-1)+"&search="+search,members.getPrevious());
        } else {
            assertNull(members.getPrevious());
        }
        if (page<1) {
            assertEquals("/users/?page="+(page+1)+"&search="+search,members.getNext());
        } else {
            assertNull(members.getNext());
        }
        assertNotNull(members.getUsers());
        assertEquals(10,members.getUsers().size());
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea0"+(page*2)+"1",members.getUsers().get(0).getId());
        assertEquals("nicknama0"+(page*2)+"1",members.getUsers().get(0).getNickName());
    }

    @Test
    public void checkUser() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/2c65ebf7-964a-47c2-8807-a871489ea004")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        User usr = objectMapper.readValue(content, User.class);
        assertEquals("2c65ebf7-964a-47c2-8807-a871489ea004", usr.getId());
        assertEquals("nickname004", usr.getNickName());

    }

}
