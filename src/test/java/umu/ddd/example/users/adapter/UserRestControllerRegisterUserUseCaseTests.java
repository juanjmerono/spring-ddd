package umu.ddd.example.users.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import umu.ddd.example.users.adapter.event.UserEventImpl;
import umu.ddd.example.users.application.port.UserPersistence;
import umu.ddd.example.users.domain.models.User;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
@AutoConfigureMockMvc
public class UserRestControllerRegisterUserUseCaseTests {
    
    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @Autowired 
    private ApplicationEvents applicationEvents;
    
    @MockBean
    private UserPersistence userPersistence;

    @BeforeEach
    public void setup() {
        Mockito.when(userPersistence.findByNickName("nickname000"))
            .thenReturn(User.builder().id("2c65ebf7-964a-47c2-8807-a871489ea000").nickName("nickname000").build());
    }

    @ParameterizedTest
    @ValueSource(strings={"nickname000","newuser2"})
    public void testAddUser(String testNick) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/users/add/?nick="+testNick)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        User usr = objectMapper.readValue(content, User.class);
        assertNotNull(usr);
        if ("newuser2".equals(testNick)) {
            assertEquals(36,usr.getId().length());
        } else {
            assertEquals("2c65ebf7-964a-47c2-8807-a871489ea000",usr.getId());
        }
        assertEquals(testNick,usr.getNickName());
        assertEquals("newuser2".equals(testNick)?1:0, applicationEvents
                .stream(UserEventImpl.class)
                .filter(event -> event.isNewUserEvent() && event.getUser().getNickName().equals(testNick))
                .count());
    }

    @Test
    public void testAddUserNoNick() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/users/add/")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @ParameterizedTest
    @ValueSource(strings={"p","ppp"})
    public void testAddInvalidUser(String invalidNick) throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put("/users/add/?nick="+invalidNick)
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

}
